# Contributing

## Modification

- Changes must have their own branch.
- Change branches must be based on `master`.
- Merge requests must target the `develop` branch.

## Style

- The code style must adhere to Rust's style standards.
- RustFMT needs to stay as configured by default.

## Tests

- The `cargo test --workspace` musn't return any warnings or errors.
- The `cargo clippy --workspace` musn't return any warnings or errors.

## Continuous Integration

- CI pipelines need to be configured correctly on forks.
- CI pipelines must pass, they contain the tests above.
- Merge requests must not have CI-disabling/ignoring markers in their title or content.
